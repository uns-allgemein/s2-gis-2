docker run 
--name "postgis" 
-p 25432:5432
-e POSTGRES_USER=mike
-e POSTGRES_PASS=123456
-e POSTGRES_DBNAME=uni
-e POSTGRES_MULTIPLE_EXTENSIONS=postgis,hstore,postgis_topology,postgis_raster,pgrouting
-v $HOME/postgres_data:/var/lib/postgresql
-d -t kartoza/postgis



Flurstück auswählen
Auswahl aller Flurstücke über Filter auf dieses begrenzen
DGM auf dieses Flurstück schneiden
Zugeschnittenes DGM zu Punkten ändern
Mittelwert aller Werte erfassen.


Beispiel:
Flur: 000
271/1;272;273/1;274

Wohnbauflächen durch geschicktes Filtern finden:
    Nur Wohnbauflächen anzeigen lassen und filtern wo keine Gebäude enthalten sind.

----
Stellungnahme
----
$ID
name        String
vorname     String
anschrift   String
telefon     String
eMail       String
baugrundstueckIn->[ListeGrundstueckIn|$ID]
Strasse->[ListeStrasse|$ID]
gemarkung->[ListeGemarkung|$ID]
flur->[ListeFlur|$ID]
flurstuecke->[StellungnahmeToFlurstuecke|$ID]
bauvorhabenArt->[ListeBauvorhaben|$ID]
eingangsdatum

----
ListeStrasse
----
$ID
strassenname

----
StellungnahmeToFlurstuecke
----
$ID
stellunahmeID->[Stellungnahme|ID]
flurstueckID->[ListeFlurstuecke|ID]

----
ListeFlurstuecke
----
$ID
flurstueck

----
ListeGemarkung
----
$ID
gemarkung

----
ListeGrundstueckIn
----
$ID
bezeichnung

----
ListeBauvorhaben
----
$ID
bezeichnung


SQL Liefert das Flurstück 272 zurück
SELECT * FROM "Flaechenmanagement"."ALKIS_flurstueck" 
WHERE flurstnr = '272'

Liefert alle Ver01
SELECT * FROM "Flaechenmanagement"."Projekt_flurstueck"

----SQL: Liefert die Flurstücke und Buffer der Flurstücke
SELECT ST_Union(geom), ST_Union(ST_Buffer(geom::geography,0.001,'quad_segs=8')) FROM "Flaechenmanagement"."Projekt_flurstueck"

### Buffer in Metern
SELECT ST_Buffer(geography(ST_Union(geom)), 500) FROM "Flaechenmanagement"."Projekt_flurstueck"

### SQL: liefert die Straßen in der nähe zurück
SELECT L.*
FROM "Flaechenmanagement"."Projekt_flurstueck" AS F,
"Flaechenmanagement"."ATKIS_ver01_l" As L
WHERE ST_CoveredBy(L.geom,ST_Buffer(F.geom::geography, 250)) = true
GROUP BY L.id

### SQL: get all Streets in 100 Meters
SELECT L.nam
FROM "Flaechenmanagement"."ALKIS_flurstueck" AS F,
"Flaechenmanagement"."ATKIS_ver01_l" As L
WHERE ST_CoveredBy(L.geom,ST_Buffer(F.geom::geography, 100)) = true 
    AND (F.flurstnr = '271/1' OR F.flurstnr = '272' OR F.flurstnr = '273/1' OR F.flurstnr = '274')
Group by L.nam

### SQL: Get Nearest Polygon
SELECT ST_ClosestPoint(L.geom,F.geom)
FROM "Flaechenmanagement"."Projekt_flurstueck" AS F,
"Flaechenmanagement"."ATKIS_ver01_l" As L
WHERE ST_CoveredBy(L.geom,ST_Buffer(F.geom::geography, 250)) = true
ORDER BY ST_Distance(ST_ClosestPoint(L.geom,F.geom), F.geom)
LIMIT 1;

### Polygon in Polygon?
SELECT L.geom, L.bezeichnung
FROM "Flaechenmanagement"."Projekt_flurstueck" AS F,
"Flaechenmanagement"."Bebauungsplan" As L
WHERE ST_CoveredBy(F.geom,L.geom) = true
Group BY L.geom, L.bezeichnung

